#!/bin/bash

echo "This script checks for IPv4 connectivity"
ping -c 1 -q -W 2 1.1.1.1 > /dev/null;
return_value=$?
if [ $return_value -eq 0 ]; then
  echo "Network is up"
else
  echo "Network is down"
   echo -n "Would you like to check again and save the output into a file? [y/n]:"
   read ans
   if [ $ans == "y" ] || [ $ans == "Y" ]; then
     ping -c 1 -W 2 1.1.1.1 > ./out.txt
   fi
fi
